function readFileContent(file) {
	const reader = new FileReader()
  return new Promise((resolve, reject) => {
    reader.onload = event => resolve(event.target.result)
    reader.onerror = error => reject(error)
    reader.readAsText(file)
  })
}

function printTopFreq() {
  var file = document.getElementById("fileToLoad").files[0];
  var answerTarget = document.getElementById('answer-target');
	readFileContent(file).then(content => {
    answerTarget.value = findMostFreq(content);
  }).catch(error => console.log(error))
}

function parseAndSort(content) {
  var lines = content.split('\n');
  var parsedLines = [];
  for (var x = 0; x < lines.length; x++)
  {
    var parsed = parseFloat(lines[x]);
    parsedLines.push(parsed);
  }
  var sortedLines = parsedLines.sort();
  return sortedLines;
}

function findMostFreq(content) {
  var sortedLines = parseAndSort(content);
  var numArray = []
  var freqArray = []

  for (var x = 0; x < sortedLines.length; x++)
  {
    var myInt = sortedLines[x];
    if (numArray.includes(myInt))
    {
      freqArray[numArray.length - 1] = freqArray[numArray.length - 1] + 1;
    } else {
      freqArray.push(1);
      numArray.push(myInt);
    }
  }

  topFive = "";
  if(freqArray.length < 5)
  {
    topFive += "There's less than 5 numbers in your file"
  }
  else
  {
    for (var k = 0; k < 5; k++)
    {
      maxFreq = Math.max(...freqArray);
      console.log(maxFreq);
      index = freqArray.indexOf(maxFreq);
      console.log(index);
      topFive += numArray[index] + "\n";
      freqArray[index] = -1;
    }
  }

  return topFive;
}
